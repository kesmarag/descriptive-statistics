## 1ο εργαστήριο 05-02-2020
[1st_lab.pdf](./lectures/1st_lab.pdf)

## 2ο εργαστήριο 12-02-2020
[titanic_300.csv](./lectures/titanic_300.csv)

[2nd_lab_instructions.pdf](./lectures/2nd_lab_instructions.pdf)

[2nd_lab.pdf](./lectures/2nd_lab.pdf)

## 3ο εργαστήριο 19-02-2020
[3rd_lab_instructions.pdf](./lectures/3rd_lab_instructions.pdf)

## 4o εργαστήριο 26-02-2020
[4th_lab_instructions.pdf](./lectures/4th_lab_instructions.pdf)
